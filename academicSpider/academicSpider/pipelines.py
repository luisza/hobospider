# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
"""
Scrapy proporciona la funcionalidad de Pipelines_ mediante la cual cada item obtenido por una araña
es procesado por diferentes clases de forma secuencias, estas pueden hacer funciones con los items como 
limpieza de HTML, eliminación de duplicados e inserción en una base de datos de consulta.

El orden en que se ejecutan las comprobacines es:

    * NoRepetidosPipeline 
    * CostarricensePipeline
    * PDFPipeline
    * DjangoArticuloPipeline
    * IndexPipeline

.. _Pipelines: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
"""

from django.utils.text import slugify
from scrapy.exceptions import DropItem
from academico.models import Articulo, Palabra_clave, Autor
from academico.utils import get_query_parametros, split_name
from tse.models import PersonaTse
from scrapy.contrib.pipeline.files import FilesPipeline
from django.conf import settings


class NoRepetidosPipeline(object):
    """
    Elimina los artículos que ya se encuentran en la base de datos, esta clase es la primera del 
    pipeline por lo que aumenta la eficacia del sistema al no tener que procesar todo el item 
    cuando está repetido
    """
    def process_item(self, item, spider):
        slug = slugify(item['titulo'])
        if Articulo.objects.filter(slug=slug).exists():
            raise  DropItem("Duplicate item found: %s" % slug)

        return item

class DjangoArticuloPipeline(object):
    """
    En esta clase se construye el objeto en la base de datos para que sea manejada con Django.
    el atributo **slug** es el identificador del documento y se genera usando la función *slugify*,
    esta reemplaza los espacios por guiones y elimina caracteres no convencionales.
    
    Cuando se guarda un artículo en la base de datos se crea un número id (también conocido como pk), el cual 
    se agrega al item, para que el indexador pueda asociar el contenido del item a un artículo en la base de 
    datos.
    
    """
    def process_item(self, item, spider):
        slug = slugify(item['titulo'])
        if Articulo.objects.filter(slug=slug).exists():
            raise  DropItem("Duplicate item found: %s" % slug)
        if 'files'  not in item:
            raise  DropItem("Files item not found: %s" % slug)
        
        articulo = Articulo(
                        slug=slug,
                        titulo=item['titulo'],
                        descripcion=item['descripcion'],
                        fecha_descarga=item['fecha_descarga'],
                        urls_obtenido=" ".join(item['file_urls']),
                        file=self.get_image_path(item['files'])         
                    )
        articulo.save()
        autores = self.get_autores(item['autores'], articulo)
        palabras_claves = self.get_palabras_claves(item['palabras_clave'], articulo)

        autores = []
        for autor in item['autores']:
            autores.append(str(autor))
        
        item['palabras_clave'] = palabras_claves
        item['autores'] = autores
        item['articulo_id'] = articulo.pk
        return item

    def get_image_path(self, file):
        return file[0]['path']
    
    
    def get_palabras_claves(self, palabras, articulo):
        if palabras:
            palabras = palabras.replace(";", ",")
        else:
            palabras = ''
        palabras_clave = []
        for pal in palabras.split(','):
            pal = pal.lstrip()
            if pal:             
                palabra = Palabra_clave.objects.get_or_create(descripcion=pal)
                articulo.palabras_claves.add(palabra[0])
                palabras_clave.append(pal)
        return palabras_clave
    
    def get_autores(self, autores, articulo):
        for autor in autores:
            articulo.autores.add(autor)


class CostarricensePipeline(object):
    """
    Este Pipeline permite buscar los nombres de los autores en una base de datos de 
    nombres costarricenses proporcionada por el TSE_ del padrón electoral.
    
    En esta clase el sistema hace una limpieza de los nombres con el fin de tener el nombre
    de mejor ajuste.
    
    .. note::
        La base de datos del TSE es bastante desordenada y sus datos están bastante sucios por lo que
        se necesita hacer busquedas hasta con 3 combinaciones de posibles nombres antes de decir que la 
        persona no se ha encontrado
        
    .. note:: 
        Si se encuentra más de una persona con el mismo nombre se le atribuye el artículo a todos 
        en la vista de aval se puede eliminar los nombres que no correspondan al artículo
        
    .. warning::
        Los nombres en la base de datos del TSE no contienen tíldes ni caracteres no ascii excepto la ñ
        
    .. _TSE: http://www.tse.go.cr/descarga_padron.htm 
    """
    def process_item(self, item, spider):
        lautores = []
        autores = []
        if ', ' in item['autores']:
            lautores += item['autores'].split(', ')
        else:
            lautores.append(item['autores'])
        
        titulo_grado = ['ed.d.', 'mis ', 'mati.', 'mati ', 'dr.', 'dra.', u'máster', 'sra.', 'sr.', 'sra ', 'sr ', 'ma.']
        
        for autor in lautores:
            autor = autor.encode('utf-8').lower()
            if  'la editora'.encode('utf-8') != autor and "la editorial".encode('utf-8') != autor:
                for ex in titulo_grado:
                    autor = autor.replace(ex.encode('utf-8').lower(), '')
                    
                autor = autor.strip()
                autores.append(autor)
                
        if not autores:
            raise DropItem(u"Nombre inválido o inexistente")
        
        list_autores = []
        for aut in autores:  # Busco primero si ya está el autor
            ok = self.get_lista_autor(aut, list_autores)
            if not ok:
                ok = self.get_lista_autor(self.no_tildes(aut), list_autores)
                if not ok:
                    print "No existe ", aut

        if not list_autores:
            raise DropItem(u"Nombre no registrado en el TSE")   
        item['autores'] = list_autores               
        return item 
        
    def no_tildes(self, texto):
        texto = texto.decode('utf-8')
        for letra in [
                      (u'á', 'a'),
                      (u'é', 'e'),
                      (u'í', 'i'),
                      (u'ó', 'o'),
                      (u'ú', 'u'),
                      ]:
            texto = texto.replace(letra[0], letra[1])

        return texto
    
    def get_lista_autor(self, autor, list_autores):
        
        nombre = get_query_parametros(split_name(autor))
        query_aut = Autor.objects.filter(**nombre)
        if not query_aut.exists():  # Si no está entonces lo busco en la el tse
            # print "Buscando  tse", nombre
            query_aut = PersonaTse.objects.filter(**nombre)
            if query_aut.exists():  # Si existe creo el autor y lo guardo en la lista de autores
                for persona in query_aut:
                    list_autores.append(
                        Autor.objects.create(
                            cedula=persona.cedula,
                            nombre=persona.nombre,
                            apellido_primero=persona.apellido_primero,
                            apellido_segundo=persona.apellido_segundo                   
                                            )
                    )
            else:  # No se encontró esta persona
                return False
        else:  # soy un autor ya registrado
            # print "Esta en los autores"
            for persona in query_aut:
                 list_autores.append(persona)        
        return True

class PDFPipeline(FilesPipeline):
    """
    Permite descargar archiculos en pdf, se asegura que todos los archivos tengan extención
    **.pdf**.  Guarda en el atributo ``files`` un diccionario con 3 cosas, la fecha de última descarga, 
    un hash de la url desde donde se descargó y la ruta donde se encuentra el archivo.  
    
    Usa FilesPipeline_ por lo que trabaja de forma eficiente.
    
    .. _FilesPipeline: http://scrapy.readthedocs.org/en/latest/topics/media-pipeline.html?highlight=filespipeline#using-the-files-pipeline
    """
    def file_path(self, request, response=None, info=None):
        dev = super(PDFPipeline, self).file_path(request, response=response, info=info)
        if dev[:-4] != '.pdf':
            dev = dev + '.pdf'
        return dev
    

class IndexPipeline(object): 
    """
    Permite indexar los articulos recuperados.
    En este Pipeline se guarda en un ídice siguientes los atributos de cada item: 
    
        * titulo
        * descripción 
        * articulo_id
        
    Este pipeline es el último que se ejecuta.
    """ 
    def process_item(self, item, spider):
        index = settings.INDICE
        writer = index.writer()
        writer.add_document(titulo=item['titulo'], descripcion=item['descripcion'],
                        pk=unicode(item['articulo_id']))
        writer.commit() 
        return item  
        