# -*- coding: utf-8 -*-

# Scrapy settings for academicSpider project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

import os
import sys

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DJPROJECT = os.path.dirname(os.path.join(BASE_DIR, '../djacademico/'))

BOT_NAME = 'academicSpider'

SPIDER_MODULES = ['academicSpider.spiders']
NEWSPIDER_MODULE = 'academicSpider.spiders'

ITEM_PIPELINES = {
                  'academicSpider.pipelines.IndexPipeline': 500,
                  'academicSpider.pipelines.DjangoArticuloPipeline': 400,
                  'academicSpider.pipelines.PDFPipeline': 300,
                  'academicSpider.pipelines.CostarricensePipeline': 200,
                  'academicSpider.pipelines.NoRepetidosPipeline': 100,
                  }

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'academicSpider RI-ECCI'
sys.path.append(DJPROJECT)

FILES_STORE = os.path.dirname(os.path.join(BASE_DIR, 'downloads/'))



os.environ['DJANGO_SETTINGS_MODULE'] = 'djacademico.settings'
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()