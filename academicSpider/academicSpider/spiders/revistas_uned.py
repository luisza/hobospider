# -*- coding: utf-8 -*-
import scrapy
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from academicSpider.spiders.ojs import Base_Open_Journal_Systems

REVISTA = "\w+"  # \W+    repertorio
BASE_URL = "http://investiga.uned.ac.cr/revistas/index.php/"


class RevistasUnedSpider(CrawlSpider, Base_Open_Journal_Systems):
    name = 'revistas_uned'
    allowed_domains = ['investiga.uned.ac.cr']
    start_urls = ['http://investiga.uned.ac.cr/revistas/']

    rules = (
            Rule(LinkExtractor(
                            allow=(BASE_URL + REVISTA + "$",
                                  BASE_URL + REVISTA + '/issue/archive',
                                  BASE_URL + REVISTA + '/issue/view/\d+',
                                  ),
                           
                            deny=('index.php/index',
                                 BASE_URL + "\w+/user/register",),
                            ), callback='do_nothing', follow=True),
#             Rule(LinkExtractor(allow=(BASE_URL + REVISTA + '/article/view/\d+/\d+',),
#                                ), callback='extract_article', follow=True),
            Rule(LinkExtractor(allow=(BASE_URL + REVISTA + '/article/view/\d+/?$',),
                               ), callback='process_article', follow=True)
    ) 
