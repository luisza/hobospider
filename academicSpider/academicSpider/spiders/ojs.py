# -*- coding: utf-8 -*-
from academicSpider.items import DocumentoItem 
from datetime import datetime


class Base_Open_Journal_Systems(object):
    """
    Recupera la información de los sitios (BASE_URL):
    
        * http://tecdigital.tec.ac.cr/servicios/ojs/index.php/
        * http://www.revistas.ucr.ac.cr/
        * http://www.revistas.una.ac.cr/
        * http://investiga.uned.ac.cr/revistas/
    
    Estos sitios utilizan el software Open_Journal_Systems (OJS_ ) y tienen las mismas especificaciones
    
    Las siguientes expresiones regulares representan los links que la araña debe seguir:
        - ``BASE_URL + \w+/issue/archive``
        - ``BASE_URL + \w+/issue/view/\d+``
        
    Las urls que tienen la información del articulo tienen el siguiente formato:
        - ``BASE_URL + \\w+/article/view/\d+/?$``
        
    En estas revistas la información está bastante bien estructurada, por lo que no es complejo extraerla.
    
    Es del tipo: `scrapy.contrib.spiders.CrawlSpider`
    
    .. _OJS: https://pkp.sfu.ca/ojs/
    """
    
    def do_nothing(self, response):
        pass
    
    def process_article(self, response):
        
        item = DocumentoItem()
        item['titulo'] = response.xpath("//*[@id='articleTitle']/h3/text()").extract()[0]
        item['autores'] = response.xpath("//*[@id='authorString']/em/text()").extract()[0]
        item['descripcion'] = "\n".join(response.xpath("//*[@id='articleAbstract']/div//text()").extract())
        item['palabras_clave'] = "\n".join(response.xpath("//*[@id='articleSubject']/div//text()").extract())
        item['fecha_descarga'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        
        # "//*[@id='content']/a/@href"
        url = response.xpath("//*[@id='articleFullText']/a/@href").extract()
        if not url:
            url = response.xpath("//*[@id='content']/a/@href").extract()
            
        if url:
            url = url[0]
            if '/view/' in url:
                url = url.replace("/view/", "/download/")
            item['file_urls'] = [url, ]
            yield item
    
    
    
    def extract_article(self, response):
        print "--------------------- ARTICULO", response.url
        item = DocumentoItem()
        item['file_urls'] = [str(response.url).replace('/view/', "/download/")]
        return item
    
