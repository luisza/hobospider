# -*- coding: utf-8 -*-
import scrapy
import re
import hashlib
from scrapy.utils.misc import md5sum
from datetime import datetime
from scrapy.conf import settings
from academicSpider.items import DocumentoItem 


class ScieloSpider(scrapy.Spider):
    """
    Scielo es una revista científica electrónica online que tiene un amplio catálogo de articulos
    de varios países y en varios idiomas.
    
    Esta araña busca artículos mediante el filtro "Costa Rica", por eso su la ruta de inicio es
    http://search.scielo.org/?q=&where=CRI, además hace uso de la funcionalidad de la página de exportar 
    pdf para descargar el artículo.
    
    Es del tipo `scrapy.Spider`
    
    """
    name = "scielo"
    allowed_domains = ["scielo.sa.cr"]
    start_urls = (
        'http://search.scielo.org/?q=&where=CRI',
    )

    def parse(self, response):
        last = response.xpath("//*[@class='pagination']/span[12]/a/@href").extract()[0]
        num_page = int(re.findall(r"\d+", last)[0])
        for num in xrange(num_page + 1):
            url = "http://search.scielo.org/?output=site&lang=pt&from=21&sort=&format=abstract&count=20&fb=&page=%d&q=&index=&where=CRI" % (num)
            yield scrapy.Request(url=url,
                                    dont_filter=True,
                                    callback=self.parse_pages)
    
    
    def get_esp(self, l):
        lis = []
        for x in l:
            y = x.replace("\n", "").strip()
            if y:
                return y 
    def parse_pages(self, response):
        for divs in response.xpath("//*[@class='data']"):
            titulo = divs.xpath("h3/a/text()").extract()[0].strip().replace("\n", "")
            autores = divs.xpath("*[@class='author']/a/text()").extract()
            descripcion = self.get_esp(divs.xpath("div[4]/text()").extract())
            for x in divs.xpath("div[4]/a/@onclick").extract():
                if "_es" in x:
                    id = x.split(";")[0].replace("').show()", "").replace("$('#", "")
                    descri = "\n".join(response.xpath("//*[@id='" + id + "']/text()").extract())
                    descripcion += " " + descri.strip()

            url = divs.xpath("div[5]/div/a/@href").extract()[0]
            
            item = DocumentoItem()
            item['titulo'] = titulo
            item['autores'] = autores[0]
            item['descripcion'] = descripcion
            item['palabras_claves'] = ""
            item['fecha_descarga'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            # self.get_articule_pdf(item, url)
            request = scrapy.Request(url=self.get_url_pdi(url),
                                     dont_filter=True,
                                     callback=self.articulo_to_pdf)
            
            request.meta['item'] = item
            yield request


    def get_url_pdi(self, url):
        a = url.split("&")
        pid = [ x for x in url.split("&") if 'pid' in x][0].replace('pid=', '')
        return "http://www.scielo.sa.cr/scielo.php?script=sci_pdf&pid=%s&lng=en&nrm=iso&tlng=es" % pid

    def articulo_to_pdf(self, response):
        ll = response.body.split("<script>")
        redir = [x for x in ll if  "setTimeout(function(){ window.location=" in x ][0]
        direction = redir.split('"')[1]
        item = response.meta['item']
        item['file_urls'] = [direction]
        
        return item
