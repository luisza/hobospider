# -*- coding: utf-8 -*-
import scrapy
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from academicSpider.spiders.ojs import Base_Open_Journal_Systems

REVISTA = "\w+"  # "\W+"   bibliotecas
BASE_URL = "http://www.revistas.una.ac.cr/index.php/"


class RevistasUnaSpider(CrawlSpider, Base_Open_Journal_Systems):
    name = 'revistas_una'
    allowed_domains = ['www.revistas.una.ac.cr']
    start_urls = ['http://www.revistas.una.ac.cr/']
    
    rules = (
            Rule(LinkExtractor(
                            allow=(BASE_URL + REVISTA + "$",
                                  BASE_URL + REVISTA + '/issue/archive',
                                  BASE_URL + REVISTA + '/issue/view/\d+',
                                  ),
                           
                            deny=('index.php/index',
                                 BASE_URL + "\w+/user/register",),
                            ), callback='do_nothing', follow=True),
#             Rule(LinkExtractor(allow=(BASE_URL + REVISTA + '/article/view/\d+/\d+',),
#                                ), callback='extract_article', follow=True),
            Rule(LinkExtractor(allow=(BASE_URL + REVISTA + '/article/view/\d+/?$',),
                               ), callback='process_article', follow=True)
    )     
