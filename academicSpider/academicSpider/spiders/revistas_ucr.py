# -*- coding: utf-8 -*-
import scrapy
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from academicSpider.spiders.ojs import Base_Open_Journal_Systems

REVISTA = "\w+"  # \W+    pharmaceutical
BASE_URL = "http://www.revistas.ucr.ac.cr/index.php/"

class RevistasUcrSpider(CrawlSpider, Base_Open_Journal_Systems):
    name = "revistas_ucr"
    allowed_domains = ["revistas.ucr.ac.cr"]
    start_urls = [
        'http://www.revistas.ucr.ac.cr/',
    ]
    
   # index.php / chibcha / issue / view / 1771

    rules = (
            Rule(LinkExtractor(
                            allow=(BASE_URL + REVISTA + "$",
                                  BASE_URL + REVISTA + '/issue/archive',
                                  BASE_URL + REVISTA + '/issue/view/\d+',
                                  ),
                           
                            deny=('index.php/index',
                                 BASE_URL + "\w+/user/register",),
                            ), callback='do_nothing', follow=True),
             Rule(LinkExtractor(allow=(BASE_URL + REVISTA + '/article/view/\d+/?$',),
                               ), callback='process_article', follow=True)
    )        
        
