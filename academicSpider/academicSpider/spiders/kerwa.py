# -*- coding: utf-8 -*-
import scrapy
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from academicSpider.items import DocumentoItem 
from datetime import datetime

URL_PAGINA = "http://kerwa.ucr.ac.cr"

class KerwaSpider(CrawlSpider):
	"""
	Kerwa almacena, difunde y preserva la producción científica y académica de la Universidad de Costa Rica. 
	Ofrece acceso abierto a libros, documentos técnicos y de trabajo; artículos, tesis, 
	registros de audio y vídeo, informes de investigación, entre otros.
	
	La ruta de inicio de esta araña es:
	http://kerwa.ucr.ac.cr/browse?order=ASC&rpp=100&sort_by=1&etal=-1&offset=0&type=title
	
	Esta araña es del tipo: `scrapy.contrib.spiders.CrawlSpider`
	"""
	name = "kerwa"
	allowed_domains = ["kerwa.ucr.ac.cr"]
	start_urls = (
		'http://kerwa.ucr.ac.cr/browse?order=ASC&rpp=100&sort_by=1&etal=-1&offset=0&type=title',
	)

	rules = (
		Rule(LinkExtractor(
			allow=('http://kerwa.ucr.ac.cr/browse?order=ASC&rpp=100&sort_by=1&etal=-1&offset=\d+&type=title')
		), follow=True),
		Rule(LinkExtractor(allow=('http://kerwa.ucr.ac.cr/handle/\d+/\d+/?$')), callback='procesar_doc', follow=True),
	)

	
	def formatearAutor(self, autor):
		c = autor.split(',')
		return c[-1] + " " + c[0]


	def limpiarAutores(self, lista):
		resultado = []
		for elem in lista:
			if ';' in elem:
				lis = elem.split(';')
				for el in lis:
					resultado.append(self.formatearAutor(el))	
			else:
				resultado.append(self.formatearAutor(elem))	
		return resultado
	
	
	def limpiarDescripcion(self, descripcion):
		for elem in descripcion:
			if elem == '\n':
				indice = descripcion.index(elem)
				del descripcion[indice]		
	
	
	def procesar_doc(self, response):
		doc = DocumentoItem()
		doc['titulo'] = response.xpath('//*[@id="aspect_versioning_VersionNoticeTransformer_div_item-view"]/div/h1/text()').extract()
		if response.xpath('//*[@class="simple-item-view-authors"]/span/text()').extract():
			doc['autores'] = self.limpiarAutores(response.xpath('//*[@class="simple-item-view-authors"]/span/text()').extract())
		else:
			doc['autores'] = self.limpiarAutores(response.xpath('//*[@class="simple-item-view-authors"]/text()').extract())
		descripcion = response.xpath('//*[@class="simple-item-view-description"]/div/text()').extract()
		self.limpiarDescripcion(descripcion)
		if len(descripcion) > 1:
			doc['descripcion'] = descripcion[0]
		else:
			doc['descripcion'] = descripcion
		doc['palabras_clave'] = ''
		doc['fecha_descarga'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
		urls = response.xpath('//*[@class="file-link"]/a/@href').extract()
		if urls:
			doc['file_urls'] = URL_PAGINA + urls[0]
		else:
			doc['file_urls'] = []