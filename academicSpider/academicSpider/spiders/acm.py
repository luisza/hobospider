# -*- coding: utf-8 -*-
import scrapy
from academicSpider.items import DocumentoItem 
from datetime import datetime

class ACMSpider(scrapy.Spider):
	"""
	ACM cuenta con un amplio catálogo de artículos científicos de varios países, 
	desgraciadamente no todos son públicos por lo que esta araña solo recupera los que tienen 
	acceso público, también filtra solo los artículos según la palabra Costa Rica.
	
	Su ruta de inicio es: http://dl.acm.org/results.cfm?query=costa%20rica
	
	Es del tipo: `scrapy.Spider`
	"""
	name = "acm"
	allowed_domains = ["http://dl.acm.org/"]
	start_urls = (
		'http://dl.acm.org/results.cfm?query=costa%20rica',
	)

	# rules = ()

	def getDescripcion_PalabClave(self, item):
		pc = ''
		contenido = item.xpath("tr[5]/td/div[@class='abstract2']/text()").extract()
		des = contenido[1].replace("\r\n", "").replace("\t", "").strip()
		if des == '':
			des = item.xpath("tr[5]/td/div[@class='abstract2']/p/text()").extract()[0].replace("\r\n", "").replace("\t", "").strip()
		if len(contenido) > 2:
			pc = contenido[2].replace("\r\n", "").replace("\t", "").strip()
		return des, pc

	def getAutores(self, item):
		if len(item.xpath("tr[1]/td/div[@class='authors']/a/text()").extract()) > 0:
			auth = item.xpath("tr[1]/td/div[@class='authors']/a/text()").extract()
		else:
			auth = item.xpath("tr[1]/td/div[@class='authors']/text()").extract()[0].replace("\r\n", "").replace("\t", "").strip()	
		return auth	


	def parse(self, response):
		p = 0
		for item in response.xpath("/html/body/div/table/tr[3]/td/table/tr[3]/td[2]/table/tr/td[2]/table"):
			if len(item.xpath("tr[4]/td/table/tr[1]/td[1]/table/tr/td[2]/a").extract()) > 0 and \
			'PDF' in item.xpath("tr[4]/td/table/tr[1]/td[1]/table/tr/td[2]/a/text()").extract()[1]:
				p += 1
				doc = DocumentoItem()
				doc['titulo'] = item.xpath("tr[1]/td/a/text()").extract()[0]
				doc['autores'] = self.getAutores(item)
				doc['descripcion'], doc['palabras_clave'] = self.getDescripcion_PalabClave(item)
				doc['fecha_descarga'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
				doc['file_urls'] = item.xpath("tr[4]/td/table/tr[1]/td[1]/table/tr/td[2]/a/@href").extract()
				print doc
				print
		print p

