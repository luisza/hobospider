# -*- coding: utf-8 -*-
import scrapy
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.spiders import CrawlSpider, Rule
from academicSpider.spiders.ojs import Base_Open_Journal_Systems


REVISTA = "\w+"  # \W+     tec_empresarial
BASE_URL = 'http://tecdigital.tec.ac.cr/servicios/ojs/index.php/'

class RevistasTecSpider(CrawlSpider, Base_Open_Journal_Systems):
    name = 'revistas_tec'
    allowed_domains = ['tecdigital.tec.ac.cr']
    start_urls = ['http://tecdigital.tec.ac.cr/servicios/ojs/index.php/']

    rules = (
            Rule(LinkExtractor(allow=(BASE_URL + REVISTA + "$",
                                  BASE_URL + REVISTA + '/issue/archive',
                                  BASE_URL + REVISTA + '/issue/view/\d+',
                                  ),
                           
                            deny=('index.php/index',
                                BASE_URL + "\w+/user/register",),
                            
                            ), callback='do_nothing', follow=True),
#             Rule(LinkExtractor(allow=(BASE_URL + REVISTA + '/article/view/\d+/\d+',),
#                                ), callback='extract_article', follow=True),
            Rule(LinkExtractor(allow=(BASE_URL + REVISTA + '/article/view/\d+/?$',),
                               ), callback='process_article', follow=True)
            )


        
