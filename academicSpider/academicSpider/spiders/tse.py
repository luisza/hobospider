# -*- coding: utf-8 -*-
import scrapy
BASE_URL = "http://www.tse.go.cr/revista/"

from academicSpider.items import DocumentoItem 
from datetime import datetime
import re

class TseSpider(scrapy.Spider):
    """
    Busca en http://www.tse.go.cr/revista/anteriores.htm
    la cual tiene la lista de articulos publicados por el TSE
    
    Es del tipo: `scrapy.Spider`
    """
    name = "tse"
    allowed_domains = ["tse.go.cr"]
    start_urls = (
        'http://www.tse.go.cr/revista/anteriores.htm',
    )

    def parse(self, response):
        for a in response.xpath("//*/td/a[2]"):
            if "Art" in a.xpath("text()").extract()[0]:
                request = scrapy.Request(url=BASE_URL + a.xpath("@href").extract()[0],
                                dont_filter=True,
                             callback=self.parse_link)
                yield request

    
    def clean_autor(self, autores):
        dev = ""
        pattern = re.compile("\d+")
        panama = u"Panam\xe1"
        mexico = u"M\xe9xico"
        peru = u"Peru"
        for autor in autores:
            if not "(" in autor and not "/" in autor\
            and not panama in autor and not mexico in autor\
            and not peru in autor and not "Panama" in autor\
            and not "Mexico" in autor and not "Universidad de Salamanca" in autor\
            and not "Universidad de Costa Rica" in autor:
                if not re.findall(pattern, autor):
                    dev = autor.replace("\r\n", "").lstrip()
        return dev
    def parse_link(self, response):      
        docs = response.xpath("//*/table/tr[3]/td/table/tr/td/table/tr/td/div/p")
        
        for doc in docs:
            laut = doc.xpath("text()").extract()
            autores = self.clean_autor(laut)
            if autores:
                item = DocumentoItem() 
                item['titulo'] = doc.xpath("a/text()").extract()[0]
                item['autores'] = autores
                item['descripcion'] = item['titulo']
                item['palabras_claves'] = ""
                item['fecha_descarga'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                item['file_urls'] = [BASE_URL + doc.xpath("a/@href").extract()[0] ]
                yield item
        
