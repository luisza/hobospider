# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy import Field


class DocumentoItem(scrapy.Item):
    """
    Este objeto permite guardar la información recolectada 
    por las arañas, los campos básicos son:
    
        * **Titulo:** str
        * **autores:**  lista o str
        * **descripción:** str 
        * **palabras claves:** lista o str
        * **fecha_descarga:**  str con formato ej. %Y-%m-%d %H:%M:%S
        * **file_urls:** lista de urls de articulos
        * **files:**  lista de archivos descargados (se llena automáticamente)
        * **articulo_id:** identificador del artículo para django.
        
    Este objeto cumple con las especificaciones de scrapy para los items ( module-scrapy.item_ )
    
    .. _module-scrapy.item: http://scrapy.readthedocs.org/en/latest/topics/items.html#module-scrapy.item
    """
    titulo = Field()
    autores = Field()
    descripcion = Field()
    palabras_clave = Field()
    fecha_descarga = Field()
    file_urls = Field()
    files = Field()
    articulo_id = Field()

