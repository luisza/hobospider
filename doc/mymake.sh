#!/bin/bash

export PYTHONPATH=$PYTHONPATH:$(cd `pwd`/../academicSpider && pwd):$(cd `pwd`/../djacademico && pwd)
export DJANGO_SETTINGS_MODULE=djacademico.settings
echo "###########  PATH   ##############"
echo $PYTHONPATH
echo "#########################"

if [ $1 == 'html' ]; then
    make $1
fi

if [ $1 == 'pdf' ]; then
    sphinx-build -b pdf source build/pdf
fi
