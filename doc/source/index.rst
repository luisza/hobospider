.. Repositorio de Artículos Académicos documentation master file, created by
   sphinx-quickstart on Wed Jun 24 10:56:47 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenido a la Documentación del Repositorio de Artículos Académicos Costarricenses
=====================================================================================

Contenido:
___________

Manual de Usuario
'''''''''''''''''

.. toctree::
   :maxdepth: 3
   
   manualUsuario

Instalación del Sistema
'''''''''''''''''''''''
.. toctree::
   :maxdepth: 3

   instalacion

Documentación de Código Fuente
''''''''''''''''''''''''''''''

.. toctree::
   :maxdepth: 3

   spiders
   interfazWeb


