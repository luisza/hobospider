Recuperación de Información de Sitios en Internet
==================================================

Para este proyecto se utilizó Scrapy_ como biblioteca para hacer web scraping, 
construyeron varias arañas para diferentes sitios que identificamos como sitios
de revistas donde podrían encontrarse autores costarricenses.

La arquitectura diseñada es :

.. image:: _images/arquitectura.png
           :height: 400 px
           :width: 800 px
           :alt: Arquitectura del sistema
           :align: center



.. _scrapy: http://scrapy.org/


Items
______

.. automodule:: academicSpider.items
    :members:
    :member-order: bysource
    :noindex:

Pipelines
___________

.. automodule:: academicSpider.pipelines
    :members:
    :member-order: bysource
    :noindex:


Arañas
_______

Antes de construir las arañas se identificó cuales son los sitios donde se publican
más articulos costarricenses, se buscó en las universidades públicas por sus 
repositorios de revistas y en algunos otros catálogos públicos en línea.
El resultado fue una gran colección de artículos por recuperar.


TSE
-------

.. automodule:: academicSpider.spiders.tse
    :members:
    :member-order: bysource
    :noindex:

Repositorios de revistas UNA, UCR, UNED, TEC
-----------------------------------------------

.. automodule:: academicSpider.spiders.ojs
    :members:
    :member-order: bysource
    :noindex:

Kerwa
-----------------------------------------------

.. automodule:: academicSpider.spiders.kerwa
    :members:
    :member-order: bysource
    :noindex:

Scientific Electronic Library Online (Scielo) 
-----------------------------------------------

.. automodule:: academicSpider.spiders.scielo
    :members:
    :member-order: bysource
    :noindex:

ACM Digital Library
-----------------------------------------------

.. automodule:: academicSpider.spiders.acm
    :members:
    :member-order: bysource
    :noindex:
