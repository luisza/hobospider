Interfaz Web
============

Página Principal
_________________

Es la encargada de mostrarle al usuario la posibilidad de ver, buscar y descargar los artículos que posee el sistema y que cuya publicación
ha sido avalada por parte de uno de sus autores como mínimo. Esta compuesta por dos partes:

Listado de Artículos
'''''''''''''''''''''

.. autoclass:: academico.articulosView.ArticuloListView
    :members:
    :undoc-members:
    :member-order: bysource
    :noindex:

Formulario de Búsqueda
'''''''''''''''''''''''

.. autoclass:: academico.articulosView.BusquedaListView
    :members:
    :undoc-members:
    :member-order: bysource
    :noindex:

Aval de Publicación
___________________

Esta página permite al autor del artículo brindar (o no) permiso de publicación de su artículo de forma libre en nuestro repositorio.  Se debe pedir permiso ya que se realiza la publicación del archivo PDF que contiene el texto completo del artículo y se busca respetar los derechos de autor de dichos textos. También le permite al autor editar los metadatos del artículo que fueron recuperados con el fin de tener la información más veraz y completa posible.

.. note:: Permite además a la persona indicar, en caso de que sea necesario, que en realidad no es el autor del artículo, lo cual puede pasar en el caso de que haya dos personas con el mismo nombre registradas en el TSE y se haya tomado la equivocada al hacer la comprobación.

.. image:: _images/aval.png
           :height: 400 px
           :width: 650 px
           :alt: Aval de Publicación de Artículos
           :align: center

.. automodule:: academico.views
    :members:
    :undoc-members:
    :member-order: bysource
    :noindex:
