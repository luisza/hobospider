Instalación del sistema
=============================

Este proyecto utiliza Django y Scrapy en sus versiones más recientes, se requiere Python 2.7
y no ha sido probado en python3.

Además es desarrollado sobre Debian y derivados y solo ha sido probado en estas plataformas.

Instalación de dependencias
---------------------------------

Instale las dependencias necesarias para la compilación de lxml 

.. code:: bash

	$ apt-get install libxml2-dev libxslt-dev python-dev 
	$ apt-get install zlib1g-dev libffi-dev libssl-dev libpq-dev


Cree un entorno virtual donde alojar las librerías

.. code:: bash

	$ mkdir entornos
	$ virtualenv entornos/HoboSpider 
	
Active el entorno (siempre que salga de sesión necesitará activar el entorno).

.. code:: bash
	
	$ source entornos/HoboSpider/bin/activate

Descarge el repositorio

.. code:: bash

	$ git clone https://luisza@bitbucket.org/luisza/hobospider.git
	$ cd hobospider

Instale las dependencias del proyecto

.. code:: bash

	$ pip install -r requirement.txt

Si todo sale bien entonces ya tenemos nuestro proyecto listo para usarse

Instalación de la base de datos
---------------------------------

.. note::
    Se puede utilizar cualquier base de datos soportada por Django DB_backends_, 
    pero recomendamos utilizar postgresql

.. _DB_backends: https://docs.djangoproject.com/en/1.8/ref/databases/

.. warning:: 
    Según la documentación de Scrapy puede que no sea recomendable utilizar una 
    base de datos relacional

Para instalar postgresql en Debian siga los siguientes pasos

.. code:: bash

	$ apt-get install postgresql-9.4

No es un requisito, pero puede ser conviente instalar pgadmin3 si se desea manejar 
la base de datos de forma gráfica

.. code:: bash

	$ apt-get install pgadmin3

Por seguridad cambie la contraseña del usuario postgres

.. code:: bash
	
	$ passwd postgres
	$ su postgres
	
Cambiamos la contraseña en la base de datos

.. code:: bash
	
	$ psql postgres
	# ALTER ROLE postgres PASSWORD 'CONTRASENA_DEL_USUARIO';
	# \q
	$ exit

Cambiamos las configuraciones de acceso local comentando la línea que dice 
listen_addresses = 'localhost'  en el archivo /etc/postgresql/9.4/main/postgresql.conf

Configure pgadmin3 para conectarse al servidor de base de datos.

Necesitará crear un usuario que se identifique de la siguente manera

* usuario: hobospider
* contraseña: hobospider
* host: localhost

Puede cambiar estas configuraciones en djacademico>djacademico>settings.py en la sección de DATABASES


Ejecutando la aplicación
----------------------------

Para crear la documentación se usa el script proporcionado `mymake.sh` con el primer parámetro del make (html, pdf, epub)
ver running-the-build_ en la documentación de Sphinx_ para más detalle del make.
Por ejemplo se podría ejecutar 

.. code:: bash

    $ cd doc
    $ ./mymake.sh html


.. _running-the-build : http://sphinx-doc.org/tutorial.html#running-the-build
.. _Sphinx: http://sphinx-doc.org/

Para crear la estructura de la base de datos del programa y crear un usuario 
admistrador se debe ejecutar:

.. code:: bash

    $ cd djacademico/
    $ python manage.py migrate
    $ python manage.py createsuperuser

Después mediante el siguiente comando se puede ejecutar la interfaz web:

.. code:: bash

    $ python manage.py runserver

En la dirección http://localhost:8000/ se mostrará la interfaz web.
Poner en producción este programa queda fuera de los límites de este documento,
puede referirse a Django deployment_

.. _deployment: https://docs.djangoproject.com/en/1.8/howto/deployment/

Para ejecutar las arañas debe posicionarse en la raíz de la capeta e ir a `academicSpider`
luego debe ejecutar las siguientes ordenes:

.. code:: bash

    $ cd academicSpider
    $ scrapy crawl <araña>


Las posibles arañas son:

    * acm
    * kerwa
    * revistas_tec
    * revistas_ucr
    * revistas_una
    * revistas_uned
    * scielo
    * tse

Igual que en Django el deploy está fuera del alcance de este texto, por lo que recomendamos 
leer Scrapy deployed_

.. _deployed: http://scrapyd.readthedocs.org/en/latest/deploy.html
