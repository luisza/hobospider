Bienvenido al Manual de Usuario del Repositorio de Artículos Académicos Costarricenses.
========================================================================================

Al ingresar a nuestro repositorio se muestra la siguiente página principal:

.. image:: _images/principal.png
           :height: 450 px
           :width: 700 px
           :alt: Página Principal
           :align: center

Navegar por la Lista de Artículos
=================================

Al ingresar a la Página Principal del sistema se observa la lista completa de artículos avalados para su publicación en nuestro repositorio.  Dicha lista se encuentra páginada de tal forma que en cada página se muestre un total de 10 artículos.  Esto con el fin de evitar una navegación cansada y pesada en dichos artículos.  Para facilitar la navegación el sistema cuenta con dos secciones donde se pueden acceder al paginado, una al inicio de la lista y otra al final señalados con un óvalo rojo en la siguiente imagen:

.. image:: _images/paginado.png
           :height: 320 px
           :width: 410 px
           :alt: Paginado
           :align: center

Para acceder a una página específica solamente es necesario dar click al número de página deseado para ser redireccionado a la misma.  Por otra parte, también se pueden utilizar los botones señalados como **<<** y **>>** para ir a la primera o la última página respectivamente.


Ver Información Completa del Artículo
=====================================

Por defecto en el listado de artículos lo que se muestra es el título del artículo, aún así, se permite acceder al resto de información dando click con el ratón en dicho título (o sobre cualquier parte del rectángulo gris en el que está contenido el título) con lo cual se despliega un cuadro con el resto de información.  Cada uno de los artículos de nuestro repositorio tiene la sigueiente información: Título, autores, descripción, palabras clave y un link desde donde descargar el texto completo del artículo en pdf, dichos datos son los que se muestran al dar click sobre el título.  Un ejemplo de la vista descrita anteriormente puede verse en la siguiente imagen:

.. image:: _images/articuloDesplegado.png
           :height: 250 px
           :width: 500 px
           :alt: Información Completa del Artículo
           :align: center

.. note:: Para volver el listado a su estado original (solamente mostrando el título del artículo) y así evitar tener un exceso de información en la pantalla, solamente es necesario dar otra vez click sobre el título (o cualquier parte del rectángulo gris que contiene el título) para que la información sea colapsada.

.. note:: Al dar click sobre el icono verde (que simboliza una hoja con una flecha) al lado de **Artículo Completo** se accede al archivo PDF que contiene el texto completo del artículo que se está observando.

Filtrar Artículos por Autor / Palabras Clave
=============================================

Se permite además hacer un filtrado de artículos por medio de etiquetas (tags).  Dicho filtrado puede hacerse tanto con los autores como con las palabras clave.  Dicho filtrado permite seleccionar solamente los artículos del autor seleccionado o que poseen la palabra clave indicada.  Para hacer este filtrado solamente es necesario hacer click sobre el nombre de un autor o palabra clave en cualquiera de los artículos.  Dichas etiquetas que permiten el filtrado son de color celeste y se les coloca un subrayado cuando se apoya el cursor sobre ellas como se ve en la siguiente imagen:

.. image:: _images/filtro.png
           :height: 150 px
           :width: 450 px
           :alt: Filtro de Autor
           :align: center

Al hacer click sobre alguno de ellos la página se recarga y la lista de artículos cambia mostrando solamente aquellos artículos que coincidan con el término seleccionado.  Los resultados del filtro también se encuentran paginados, por lo que si se están viendo los artículos resultantes de la aplicación de un filtro y se pasa a la siguiente página el filtro sigue estando aplicado a los artículos de dicha nueva página.

.. note:: Si desea volver a la lista completa de artículos puede hacerlo dando click sobre el botón con el ícono de una casa que se encuentra en la esquina superior izquierda de la pantalla.

.. image:: _images/inicio.png
           :height: 100 px
           :width: 800 px
           :alt: Inicio
           :align: center
    

Realizar Búsquedas
==================

En este repositorio se permite realizar la búsqueda de artículos por dos criterios: Título y Descripción.  En la sección izquierda de la pantalla se encuentra disponible un formulario que permite indicar los términos de búsqueda.

.. image:: _images/busqueda.png
           :height: 250px
           :width: 250 px
           :alt: Búsqueda de Artículos
           :align: center

En dicho formulario se pueden ingresar palabras separadas por espacios y para realizar la búsqueda se debe presionar el botón Buscar.  La búsqueda puede realizarse solamente por título, solamente por descripción o por una combinación de ambos, en el caso de que se indiquen términos de búsqueda en los campos del formulario los resultados van a mostrar solamente los artículos que cumplen con los términos indicados en el título y la descripción a la vez (comportamiento de AND lógico).  Si alguno de los dos campos se deja en blanco solamente se realizará la búsqueda con el término indicado en el campo que sí fue llenado. \n

Los resultados de búsqueda también se encuentran paginados y se muestran en el campo del listado de artículos (parte derecha de la pantalla).

.. note:: El sistema mostrará un mensaje en el caso de que los términos de búsqueda no encuentren resultados.  

.. image:: _images/sinResultados.png
           :height: 250px
           :width: 600 px
           :alt: Búsqueda sin Resultados
           :align: center


.. note:: Al igual que cuando se aplica un filtro, si desea volver a la lista completa de artículos puede hacerlo dando click sobre el botón con el ícono de una casa que se encuentra en la esquina superior izquierda de la pantalla.


Intefaz Exclusiva para Autores
==============================

Esta interfaz le permite a los autores brindar el permiso correspondiente para la publicación de sus artículos.  En nuestro repositorio no se publicará ningún artículo que no posea permiso de publicación de al menos uno de sus autores.  El acceso de esta interfaz es exclusivo para los autores y la URL se les brindará por medio de un correo electrónico en el cual se les indicará la información sobre nuestro repositorio y se les pedirá acceder a esta interfaz y brindar el consentimiento necesario.


Avalar Publicación de Artículos 
_______________________________

La vista principal de esta sección se muestra a continuación:

.. image:: _images/aval.png
           :height: 400 px
           :width: 650 px
           :alt: Aval de Publicación de Artículos
           :align: center

Cambiar Información General del Artículo
''''''''''''''''''''''''''''''''''''''''

.. image:: _images/aval_sub.png
           :height: 400 px
           :width: 650 px
           :alt: Aval de Publicación de Artículos
           :align: center


Si usted es uno de los autores del artículo y le llegó una notificación de nuestro sistema, puede dirigirse al enlace proporcionado
y se le presentará una pantalla como la anterior, puede modificar la información de artículo a placer solo que recuerde que los autores y las palabras 
clave se guardan automáticamente, mientras que los campos enmarcados con rojo solo se guardan cuando se hace el aval del artículo.

**Le rogamos encarecidamente que avale los artículos que ha hecho, pues esto permitirá a nuestro sistema crecer y llegar a más público.**



Manipulando Autores
''''''''''''''''''''''


En la sección superior izquierda se muestra la información de los autores del articulo,
esta fue recolectada por el sistema y puede tener errores.  Se muestra el número de cédula 
de cada uno de los autores para poder distinguir los autores que tengan el mismo nombre.

.. image:: _images/agregarAutor.png 
   :height: 110 px
   :width: 350 px
   :alt: Agregar Autor
   :align: center

.. warning::
    Este sistema no puede reconocer entre dos personas con el mismo nombre, por eso agrega a ambas personas al artículo
    esperando que el autor del mismo se encargue de actualizar los metadatos y eliminar las personas que no formaron 
    parte del escrito

Para agregar un autor haga click en el botón `más` **(+)**, luego de esto aparecerá un cuadro 
donde se le pedirá el nombre y los dos apellidos del autor, ingréselos y haga click en **Guardar**, 
el sistema buscará en la base de datos el autor y lo asociará, en caso de que no esté registrado 
creará un nuevo registro para el autor.

.. image:: _images/form_agregarAutor.png 
   :height: 250 px
   :width: 400 px
   :alt: Form Agregar Autor
   :align: center 

.. note::
    El sistema solo colecta información de personas costarricenses pero usted puede agregar extranjeros si lo desea.

Para eliminar un autor haga click en el correspondiente icono de basura

.. image:: _images/borrarAutor.png 
   :height: 110 px
   :width: 350 px
   :alt: Eliminar Autor
   :align: center


Manipulando Palabras Clave
'''''''''''''''''''''''''''''

En la parte superior derecha de la pantalla se muestra la información de palabras clave del artículo, en 
esta sección podrá manipular cuales palabras claves describen el artículo y cuales no, de forma predeterminada
el sistema elige las palabras, pero el autor puede modificarlas (crear nuevas y eliminar existentes).

Si se desea modificar una palabra clave se debe eliminar primero la palabra que no se quiere e insertar la palabra deseada. 

.. image:: _images/agregarPalabraClave.png 
   :height: 150 px
   :width: 350 px
   :alt: Agregar Palabra Clave
   :align: center

Se pueden agregar palabras claves haciendo click en el botón azul con el signo `más` **(+)** que está encerrado en rojo en la foto anterior,
se levantará una ventana como la siguiente, que le pedirá ingresar una nueva palabra, debe agregar una palabra 
a la vez.

.. image:: _images/form_agregarPalabraClave.png 
   :height: 150 px
   :width: 350 px
   :alt: Form Agregar Palabra Clave
   :align: center 

Para eliminar una palabra clave haga click en el correspondiente icono de basura 

.. image:: _images/borrarPalabraClave.png 
   :height: 150 px
   :width: 350 px
   :alt: Eliminar Palabra Clave
   :align: center


Avalar la Publicación
'''''''''''''''''''''

.. image:: _images/avalarArticulo.png 
   :height: 150 px
   :width: 350 px
   :alt: Avalar Publicación
   :align: center

Si usted es el autor de la publicación y desea que dicha publicación sea incluida en nuestro repositorio puede 
avalarla haciendo Click en el botón Sí, con ello hará publico el documento y se mostrará en nuestra página.

Recuerde que avalando la publicación de sus artículos estará ayudando a crear una sociedad más inclusiva, permitiendo 
el libre acceso al conocimiento elaborado por usted.

.. note::
    Antes de avalar un articulo recuerde actualizar los metadatos del mismo y 
    verificar que toda la información desplegada esté correcta, así el artículo 
    se presentará correctamente y nos ayuda a prestar un mejor servicio.

No Avalar la Publicación
'''''''''''''''''''''''''
.. image:: _images/noAvalarArticulo.png 
   :height: 150 px
   :width: 350 px
   :alt: No Avalar Publicación
   :align: center

Si usted es el autor y no desea incluir el artículo en nuestro repositorio entonces puede indicarlo haciendo Click
en la opción No (encerrada en rojo), esto eliminará el artículo de nuestra base de datos de forma permanente y se le mostrará un mensaje de agradecimiento por el tiempo dedicado. 

.. warning::
    Utilizar esta opción limitará a otras personas tener un libre acceso a la información, 
    complicará a otros investigadores la tarea de recolección de información y 
    reducirá las posibilidades de ser reconocido. Por lo que le recomendamos **NO usar esta opción**.

Indicar que no es el Autor del Artículo
'''''''''''''''''''''''''''''''''''''''

.. image:: _images/noAutor.png 
   :height: 150 px
   :width: 350 px
   :alt: No Autor
   :align: center
   
Nuestros artículos son recuperados de diversas fuentes, las cuales representan
los nombres de manera muy diferente lo que se presta para confusiones, por lo que puede se le atribuya un articulo que no fue elaborado 
por usted, si esto ocurre puede notificarlo al sistema mediante el botón encerrado en rojo, 
así el sistema lo quitará de las listas de autores y le mostrará un mensaje de 
agradecimiento por el tiempo dedicado.

Otro caso posible es que el sistema encuentre personas con su mismo nombre y que por lo tanto lo incluya, en ese caso igual debe informar que no es el 
autor. 

