# README #

Se buscará elaborar un catálogo de documentos que podrá ser consultado por nombre de autor o por descripción, además se creará un sistema web que permita al autor de un documento ceder los permisos de publicación para que estos documentos puedan ser publicados en catálogos públicos

### Instalación del entorno ###


Este proyecto se desarrolla sobre Debian y derivados y solo es probado en estas plataformas.

Instale las dependencias necesarias para la compilación de lxml 

	$ apt-get install libxml2-dev libxslt-dev python-dev 
	$ apt-get install zlib1g-dev libffi-dev libssl-dev libpq-dev
	$ apt-get install wkhtmltopdf

Cree un entorno virtual donde alojar las librerías

	$ mkdir entornos
	$ virtualenv entornos/HoboSpider 
	
Active el entorno (siempre que salga de sesión necesitará activar el entorno).
	
	$ source entornos/HoboSpider/bin/activate

Descarge el repositorio

	$ git clone https://luisza@bitbucket.org/luisza/hobospider.git
	$ cd hobospider

Instale las dependencias del proyecto

	$ pip install -r requirement.txt

Si todo sale bien entonces ya tenemos nuestro proyecto listo para usarse

### Instalación de la base de datos ###

** Nota: ** Se puede utilizar cualquier base de datos soportada por [Django DB backends](https://docs.djangoproject.com/en/1.8/ref/databases/), pero recomendamos utilizar postgresql

** Nota: ** Según la documentación de Scrapy puede que no sea recomendable utilizar una base de datos relacional

Para instalar postgresql en Debian siga los siguientes pasos

	$ apt-get install postgresql-9.4

No es un requisito, pero puede ser conviente instalar pgadmin3 si se desea manejar la base de datos de forma gráfica

	$ apt-get install pgadmin3

Por seguridad cambie la contraseña del usuario postgres
	
	$ passwd postgres
	$ su postgres
	
Cambiamos la contraseña en la base de datos

	$ psql postgres
	# ALTER ROLE postgres PASSWORD 'CONTRASENA_DEL_USUARIO';
	# \q
	$ exit

Cambiamos las configuraciones de acceso local comentando la línea que dice listen_addresses = 'localhost'  en el archivo /etc/postgresql/9.4/main/postgresql.conf

Configure pgadmin3 para conectarse al servidor de base de datos.

Necesitará crear un usuario que se identifique de la siguente manera

* usuario: hobospider
* contraseña: hobospider
* host: localhost

Puede cambiar estas configuraciones en djacademico>djacademico>settings.py en la sección de DATABASES


