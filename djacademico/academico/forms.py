from django import forms
from academico.models import Autor, Palabra_clave

class Autor_Form(forms.ModelForm):
    class Meta:
        model = Autor
        exclude = ('cedula',)
        widgets = {
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'apellido_primero': forms.TextInput(attrs={'class':'form-control'}),
            'apellido_segundo': forms.TextInput(attrs={'class':'form-control'}),
        }

    def clean(self):
        cleaned_data = super(Autor_Form, self).clean()
        self.cleaned_data['nombre'] = self.cleaned_data.get("nombre").strip()
        self.cleaned_data['apellido_primero'] = self.cleaned_data.get("apellido_primero").strip()
        self.cleaned_data['apellido_segundo'] = self.cleaned_data.get("apellido_segundo").strip()

        
class PalabraClave_Form(forms.ModelForm):
    class Meta:
        model = Palabra_clave
        fields = ('descripcion',)
        widgets = {
            'descripcion': forms.TextInput(attrs={'class':'form-control'}),
        }
        
    def clean(self):
        cleaned_data = super(PalabraClave_Form, self).clean()
        self.cleaned_data['descripcion'] = self.cleaned_data.get("descripcion").strip()