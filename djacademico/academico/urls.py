from django.conf.urls import patterns, url, include
from academico.views import aval_View as aval
from academico.views import guardar_autor, guardar_palabraClave, eliminar_autor, eliminar_palabraClave, \
                            avalar_articulo, no_avalar_articulo, no_autor

from academico.articulosView import ArticuloListView, BusquedaListView
urlpatterns = patterns('',
                       # url('^(?P<pk>\d+)$', 'academico.views.docs', name="docs"),
                       url('^$', ArticuloListView.as_view(), name = 'articulos'),
                       url('^aval/(?P<slug>[-\w]+)$', aval, name="Aval"),
                       url('^(?P<slug>[-\w]+)/agregar_autor$', guardar_autor),
                       url('^(?P<slug>[-\w]+)/agregar_palabra$', guardar_palabraClave),
                       url('^eliminarAutor/(?P<autor>\d+)/(?P<articulo>\d+)$', eliminar_autor),
                       url('^eliminarPalabraClave/(?P<palabra>\d+)/(?P<articulo>\d+)$', eliminar_palabraClave),
                       url('^avalar_articulo/(?P<slug>[-\w]+)$', avalar_articulo),
                       url('^no_avalar_articulo/(?P<slug>[-\w]+)$', no_avalar_articulo),
                       url('^no_autor/(?P<slug>[-\w]+)/(?P<autor>\d+)$', no_autor),
                       url('^buscar/$', BusquedaListView.as_view()),
                       )
