# encoding: utf-8
'''
Created on 13/6/2015

@author: luisza
'''
from django.template.defaultfilters import title as djtitle

def split_name(name):
    dev = ['', '', '']
    lname = [x for x in name.split(" ") if len(x) > 2]
    if len(lname) < 3:

        for x, n in enumerate(lname):
            dev[x] = n
    else:
        dev[2] = lname.pop()
        dev[1] = lname.pop()
        dev[0] = " ".join(lname)

    return dev


def get_query_parametros(name):
    dev = {'nombre__icontains': name[0]}

    if name[1]:
        dev['apellido_primero__icontains'] = name[1]

    if name[2]:
        dev['apellido_segundo__icontains'] = name[2]

    return dev


def title(string):
    preposiciones = [' A ', ' Ante ', ' Bajo ', ' Como ', ' Durante ', ' Mediante ', ' Cabe ', ' Con ', ' So ',
                     ' Versus ', ' Via ', ' Contra ', ' De ', ' Del ', ' Desde ', ' En ', ' Ente ', ' Hacia ',
                     ' Para ', ' Por ', ' Pro ', u' Según ', ' Sin ', ' Sobre ', ' Tras ', ' E ',
                     ' El ', ' La ', ' Los ', ' Las ', ' O ', ' Y ', ' U ', ' I ', ' Hasta ', ' Excepto ',
                     ' Una ', ' Un ', ' Unas ', ' Unos ', ' Dentro ', u' Algún ',
                     ' Algunos ', ' Algunas ', ' Alguna ', ]


    titulo = djtitle(string)
    for pre in preposiciones:
        titulo = titulo.replace(pre, pre.lower())

    return titulo
