# -*- coding: utf-8 -*-

from django.views.generic.list import ListView
from academico.models import Articulo
from django.conf import settings
from whoosh import qparser


class ArticuloListView(ListView):
    '''
        .. image:: _images/listado.png
           :height: 350px
           :width: 480 px
           :alt: Lista de Artículos
           :align: center
        
        Esta vista es la encargada de hacer el listado inicial de todos los artículos que se muestran en la página principal
        del sistema.  Realiza además la paginación y le envía los datos a la plantilla que se encarga de proporcionarle orden 
        y estilo a dichos artículos.  Hereda de la clase Django ListView y sobreescribe algunos de sus métodos para proporcionar
        el funcionamiento deseado.   
        Además, se encarga de tomar en cuenta los tags (autores y palabras clave) en la lista de artículos a mostrar y en 
        el paginado.
        Se encarga de indicar, además, por cada uno de los artículos del listado su información importante como lo es el título, 
        descripción, autores, palabras clave y un link de descarga  del texto completo.  La plantilla es la que se encarga de 
        darle estilo a dicha información. 
        
    ''' 
    model = Articulo  # : Es el modelo creado para guardar la información de los artículos
    paginate_by = 10  # : Cada una de las páginas tiene 10 artículos

    def get_context_data(self, **kwargs):
        '''
        Es el método encargado de enviar el contexto adecuado a la plantilla, toma en cuenta los filtros (tags) seleccionados 
        por el usuario: autores y palabras clave, en la lista de artículos enviada a la plantilla para ser mostrada.  
        Si no existen filtros entonces se envía la lista completa de artículos (o en el caso de búsqueda, la lista de 
        resultados de búsqueda).
        '''
        context = super(ArticuloListView, self).get_context_data(**kwargs)
        tags = self.request.GET.get('tags', '')
        autor = self.request.GET.get('autor', '')
        filters = '?'
        if tags:
            filters += 'tags=' + tags
            context['cat_tags'] = "?tags=" + tags
        if autor:
            filters = filters + '&' if filters != '?' else '?'
            filters += 'autor=' + autor
        filters = filters + '&' if filters != '?' else '?'
        context['filters'] = filters
        return context

    def get_queryset(self):
        '''
            Devuelve el query que se debe hacer a la base de datos (por medio de los modelos) para obtener los artículos que se
            quieren mostrar al usuario en la vista principal.  En este caso, devuelve todos los artículos que posee el sistema que 
            tienen permiso para ser publicados (que fueron avalados por sus autores).
        '''
        query = super(ArticuloListView, self).get_queryset().filter(aval=True)
        tags = self.request.GET.get('tags', '')
        if tags:
            query = query.filter(palabras_claves__descripcion=tags)

        autor = self.request.GET.get('autor', '')

        if autor:
            autor = autor.split(';') or []
            query = query.filter(autores__in=autor)
        return query

    
class BusquedaListView(ArticuloListView):
    '''
        .. image:: _images/busqueda.png
           :height: 250px
           :width: 250 px
           :alt: Búsqueda de Artículos
           :align: center
        
        Esta vista es la encargada de las búsquedas de artículos (por descripción y título). Se encarga de mostrarle al usuario
        la lista de artículos (de los que se ha obtenido su aval de publicación) que coinciden con los términos de búsqueda. 
        Puede recibir uno de los campos o ambos, en el caso de que reciba ambos campos (autor y título) realiza una búsqueda
        de artículos que contengan ambos términos (un comportamiento de AND booleano).
        
        .. note:: Hereda de la clase :class:`ArticuloListView` para evitar la reimplementación del código que realiza el \
        manejo de los filtros (tags) de autor y palabras clave que puede utilizar el usuario para revisar sus artículos \
        de interés.
        
    '''
    template = "academico/articulo_list.html"  # : Indica la plantilla que debe ser renderizada cuando se invoca a esta vista con una URL

    def post(self, request, *args, **kwargs):
        '''
            Devuelve los parámetros del formulario de búsqueda recibidos por el método POST en esta vista, con esto
            se permite que se sepa los términos indicados por el usuario en la búsqueda y se pueden devolver los artículos
            adecuados con respecto a dichos términos.
        '''
        return self.get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        '''
            Se utiliza el método heredado de la clase padre :class: `ArticulosListView`, osea, este método se encarga de
            mantener el filtro por tags realizado por el usuario en la vista principal.  
            Se le agregan además los términos de búsqueda ingresados por el usuario para que al recargar la página cuando se
            realiza la búsqueda aún se encuentren disponibles en el formulario de la intefaz y el usuario pueda estar consciente
            de qué búsqueda realizó y porqué recibió los resultados que está observando.
        '''
        context = super(BusquedaListView, self).get_context_data(**kwargs)
        context['titulo'] = self.request.POST.get('titulo', '') or self.request.GET.get('titulo', '')
        context['descripcion'] = self.request.POST.get('descripcion', '') or self.request.GET.get('descripcion', '')

        if context['titulo']:
            context['filters'] += "titulo=" + context['titulo'] + "&"
        if context['descripcion']:
            context['filters'] += "descripcion=" + context['descripcion'] + "&"
        return context

    def get_queryset(self):
        '''
            Este método es el encargado de realizar la búsqueda y devolverle a la plantilla la lista de artículos que coinciden
            y deben ser mostrados al usuario.
            Se utiliza el índice creado con las descripciones y títulos de los artículos del sistema y se realiza, como ya 
            se mencionó, un AND en los términos de búsqueda ingresados por el usuario en los dos campos disponibles. 
        '''
        query = super(BusquedaListView, self).get_queryset().filter(aval=True)
        indice = settings.INDICE
        titulo = self.request.POST.get('titulo', '') or self.request.GET.get('titulo', '')
        descripcion = self.request.POST.get('descripcion', '') or self.request.GET.get('descripcion', '')
        resultado_list = []
        with indice.searcher() as searcher:
            parser = qparser.QueryParser("titulo", schema=settings.INDICE.schema, group=qparser.AndGroup)
            busqueda = "titulo:(%s) " if titulo else ""
            busqueda += "descripcion:(%s)" if descripcion else ""
            lista_term = [x for x in [titulo, descripcion] if x]

            q = parser.parse(busqueda % tuple(lista_term))
            results = searcher.search(q, limit=None)
            resultado_list = [int(result['pk']) for result in results]
        return query.filter(pk__in=resultado_list).order_by('pk')

