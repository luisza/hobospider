'''
Created on 15/6/2015

@author: luisza
'''
from django.utils.safestring import mark_safe
from django import template
register = template.Library()
from academico.utils import title
from django.core.urlresolvers import reverse

from collections import OrderedDict

"""
@register.simple_tag(takes_context=True)
def sin_autores_repetidos(context, articulo):
    autores_dic = {}
    dev = ''
    for autor in articulo.autores.all():
        if not str(autor) in autores_dic:
            autores_dic[str(autor)] = []
        autores_dic[str(autor)].append(autor.pk)

    cant_autores = len(autores_dic)

    c = 0
    for name, ids in autores_dic.items():
        c += 1
        dev += '<a href="' + reverse('articulos') + '?autor=' + ";".join(map(str, ids)) + '">' + title(name)
        if c != cant_autores:
            dev += ' - '
        dev += '</a>' 

    return mark_safe(dev)
"""

def por_palabras(str1, str2):
    lstr1 = str1.split(" ")
    lstr2 = str2.split(" ")
    min_tam = len(lstr1) if len(lstr1) < len(lstr2) else len(lstr2)


    lstr1 = list(OrderedDict.fromkeys(lstr1))
    lstr2 = list(OrderedDict.fromkeys(lstr2))

    rep = []
    num_sim = 0
    for x in lstr1:
        if x in lstr2:
            num_sim += 1 
            rep.append(x)

    if not num_sim:
        return 0, rep
    return  float(num_sim) / float(min_tam) , rep


@register.simple_tag(takes_context=True)
def sin_autores_repetidos(context, articulo):
    list_autores = list(articulo.autores.all())
    ld = len(list_autores)

    nombres = {}
    nom = ''
    cam_nom = False
    ins_pos = []
    for x in range(ld):
        if x in ins_pos:
            continue
        cam_nom = False

        for y in range(x + 1, ld):
            # simil =  string_similarity(words[x], words[y])
            simil, linom = por_palabras(str(list_autores[x]), str(list_autores[y]))
            # print words[x], ' -- ', words[y], ' --> ', simil
            if simil >= 0.5:
                cam_nom = True
                nom = " ".join(linom)
                ins_pos.append(y)
                if nom not in nombres:
                    nombres[nom] = [list_autores[y]]
                elif list_autores[y] not in nombres[nom]:
                    nombres[nom].append(list_autores[y])
        if cam_nom:
            cam_nom = False
            nombres[nom].append(list_autores[x])
        else:
            nombres[str(list_autores[x])] = [list_autores[x]]

    cant_autores = len(nombres)
    dev = ''
    c = 0
    for name, ids in nombres.items():
        c += 1
        dev += '<a href="' + reverse('articulos') + '?autor=' + ";".join([ str(x.pk) for x in ids ]) + '">' + title(name)
        if c != cant_autores:
            dev += ' - '
        dev += '</a>'

    return mark_safe(dev)