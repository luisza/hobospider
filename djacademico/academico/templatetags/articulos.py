'''
Created on 15/6/2015

@author: luisza
'''


from academico.utils import title as mytitle

from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.filter
@stringfilter
def capitalize_title(value):
    return mytitle(value)
