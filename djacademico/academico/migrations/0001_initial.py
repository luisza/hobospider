# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Articulo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('slug', models.SlugField(max_length=500)),
                ('titulo', models.CharField(max_length=500)),
                ('descripcion', models.TextField(default=b' ')),
                ('fecha_descarga', models.CharField(max_length=10)),
                ('urls_obtenido', models.CharField(max_length=300)),
                ('file', models.FileField(upload_to=b'full/')),
            ],
        ),
        migrations.CreateModel(
            name='Autor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cedula', models.CharField(max_length=15)),
                ('nombre', models.CharField(max_length=50)),
                ('apellido_primero', models.CharField(max_length=50)),
                ('apellido_segundo', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Palabra_clave',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('descripcion', models.CharField(max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='articulo',
            name='autores',
            field=models.ManyToManyField(to='academico.Autor', null=True),
        ),
        migrations.AddField(
            model_name='articulo',
            name='palabras_claves',
            field=models.ManyToManyField(to='academico.Palabra_clave', null=True),
        ),
    ]
