# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('academico', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='autor',
            name='apellido_primero',
            field=models.CharField(max_length=50, verbose_name=b'Primer Apellido'),
        ),
        migrations.AlterField(
            model_name='autor',
            name='apellido_segundo',
            field=models.CharField(max_length=50, verbose_name=b'Segundo Apellido'),
        ),
        migrations.AlterField(
            model_name='palabra_clave',
            name='descripcion',
            field=models.CharField(max_length=50, verbose_name=b'Palabra'),
        ),
    ]
