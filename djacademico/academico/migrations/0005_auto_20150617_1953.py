# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('academico', '0004_articulo_aval'),
    ]

    operations = [
        migrations.AlterField(
            model_name='articulo',
            name='autores',
            field=models.ManyToManyField(to='academico.Autor'),
        ),
        migrations.AlterField(
            model_name='articulo',
            name='palabras_claves',
            field=models.ManyToManyField(to='academico.Palabra_clave'),
        ),
    ]
