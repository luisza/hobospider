# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('academico', '0002_auto_20150614_0338'),
    ]

    operations = [
        migrations.AlterField(
            model_name='articulo',
            name='fecha_descarga',
            field=models.CharField(max_length=20),
        ),
    ]
