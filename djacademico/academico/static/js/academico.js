function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function eliminarAutor(id, articulo, elemento){
	dialogo = $('<div id="dialog-confirm" title="Eliminar">\
		<p><span class="glyphicon glyphicon-exclamation-sign" style="float:left; margin:20px 10px 20px 0;"></span>\
		Esta operaci&oacute;n es irreversible, ¿Está seguro que desea eliminar el autor?</p></div>');
		dialogo.dialog({
			resizable: false,
			height:205,
			position: { my: "left top", at: "left bottom", of: elemento },
			open: function(){
            var closeBtn = $('.ui-dialog-titlebar-close');
            closeBtn.append('<span class="glyphicon glyphicon-remove"></span>');
        	},
        	close: function(){
        		$( this ).remove();
        	},
			buttons: {
			'No': function() {
				$( this ).dialog( "close" );
			},
			'Sí': function() {
				$( this ).dialog( "close" );
				$.ajax({
					type: "POST",
					url: "/eliminarAutor/"+id+"/"+articulo,
					data: {'csrfmiddlewaretoken' : getCookie('csrftoken')},
					success: function(data){
						if(data == 'True'){
							var idB = "#" + articulo + "_" + id;
							$(idB).remove();
						}else{
							$("#mensajes").data("Ocurrió un problema eliminando el autor");
							$("#mensajes").removeClass("hidden");
						}
					},
				});
			}}
	});
}

function eliminarPalabraClave(id, articulo, elemento){
	$.ajax({
		type: "POST",
		url: "/eliminarPalabraClave/"+id+"/"+articulo,
		data: {'csrfmiddlewaretoken' : getCookie('csrftoken')},
		success: function(data){
			if(data == 'True'){
				var idB = "#" + articulo + "_" + id;
				$(idB).remove();
			}else{
				$("#mensajes").data("Ocurrió un problema eliminando la palabra clave");
				$("#mensajes").removeClass("hidden");
			}
		},
	});
}

function guardarAutor(){
	$("#agregarAutor").modal('hide');
	$("#agregarAutor .modal-body form").find("input[type=text]").val("");
}

function guardarPalabra(){
	$("#agregarPalabraClave").modal('hide');
	$("#agregarPalabraClave .modal-body form").find("input[type=text]").val('');	
}
