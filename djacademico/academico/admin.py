from django.contrib import admin
from academico.models import Articulo, Autor, Palabra_clave

# Register your models here.


class AutorAdmin(admin.ModelAdmin):
    search_fields = ('nombre', 'apellido_primero', 'apellido_segundo')


class ArticuloAdmin(admin.ModelAdmin):
    search_fields = ('titulo',)
    list_filter = ('aval',)

    actions = ['avalar', ]

    def avalar(self, request, queryset):
        queryset.update(aval=True)
    avalar.short_description = "Avalar articulo"

admin.site.register(Autor, AutorAdmin)
admin.site.register(Articulo, ArticuloAdmin)
admin.site.register(Palabra_clave)
