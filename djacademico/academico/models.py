from django.db import models
from django.utils.six import python_2_unicode_compatible
from django.core.urlresolvers import reverse
# Create your models here.

@python_2_unicode_compatible
class Palabra_clave(models.Model):
    descripcion = models.CharField(max_length=50, verbose_name="Palabra")

    def __str__(self):
        return self.descripcion

@python_2_unicode_compatible
class Autor(models.Model):
    cedula = models.CharField(max_length=15)
    nombre = models.CharField(max_length=50)
    apellido_primero = models.CharField(max_length=50, verbose_name="Primer Apellido")
    apellido_segundo = models.CharField(max_length=50, verbose_name="Segundo Apellido")

    def __str__(self):
        return self.nombre + ' ' + self.apellido_primero + ' ' + self.apellido_segundo


@python_2_unicode_compatible
class Articulo(models.Model):
    slug = models.SlugField(max_length=500)
    titulo = models.CharField(max_length=500)
    autores = models.ManyToManyField(Autor)
    descripcion = models.TextField(default=" ")
    palabras_claves = models.ManyToManyField(Palabra_clave)
    fecha_descarga = models.CharField(max_length=20)
    urls_obtenido = models.CharField(max_length=300)
    file = models.FileField(upload_to="full/")
    aval = models.BooleanField(default=False)

    def __str__(self):
        return self.titulo

    def get_absolute_url(self):
        return reverse('Aval', args=[self.slug]) + "?autor=" + str(self.autores.all().first().pk)
