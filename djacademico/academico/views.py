# -*- coding: utf-8 -*-
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from academico.models import Articulo, Autor, Palabra_clave
from academico.forms import Autor_Form, PalabraClave_Form
from django.shortcuts import render_to_response
from django_ajax.decorators import ajax
from django.template.defaultfilters import title



def aval_View(request, slug):
    '''
        Es la vista encargada de enviar a la plantilla la información que se posee del artículo sobre el cuál se está realizando
        el aval.  Realiza la búsqueda del artículo y lo envía a la plantilla como parámetro para que esta lo muestre al usuario
        con formato y orden.  Además crea los formularios que se requieren para agregar palabras claves y autores en el caso
        de que el autor que está haciendo la revisión considere que es necesario.
        
        .. note:: El slug que recibe como parámetro corresponde al identificador único que posee el artículo que permite acceder \
        a él por medio de una URL única.  Además, es utilizado como identificador para hacer la búsqueda del artículo en la base \
        de datos.  Esto debe tomarse en cuenta para todas las demás vistas que reciben un slug como parámetro.
    '''
    articulo = get_object_or_404(Articulo, slug=slug)
    autorForm = Autor_Form()
    palabraForm = PalabraClave_Form()
    autor_id = request.GET.get('autor', 0)
    return render(request, "aval_publicacion.html", {"articulo": articulo,
                                                     "autorForm": autorForm,
                                                     "palabraForm": palabraForm,
                                                     "autor": autor_id})

@ajax
def guardar_autor(request, slug):
    '''
        .. image:: _images/agregarAutor.png 
           :height: 110 px
           :width: 350 px
           :alt: Agregar Autor
           :align: center
    
        Esta vista es la encargada de agregar nuevos autores al artículo.  Toma los datos ingresados por el usuario en el 
        formulario de la interfaz web y con ellos busca si ya existe un autor con dichos datos, si es así asocia dicho autor
        al artículo que se está visualizando (lo obtiene por medio del slug), sino crea un nuevo autor que asocia al artículo.  
        En esta vista se usa Ajax por lo que la respuesta que envía corresponde a código html (que indica si la acción tuvo éxito 
        o no) que se agrega a la vista del usuario con el fin de evitar recargar la pantalla cada vez que se agrega 
        un nuevo autor y aún así el usuario esté siempre informado sobre lo que está sucediendo en el sistema.
        
        .. note:: El formulario que permite al usuario agregar autores desde la interfaz es el que se muestra en la siguiente \
        imagen.  Los datos ingresados en él son los que se reciben como parámetros del POST en esta vista.
        
        .. image:: _images/form_agregarAutor.png 
           :height: 250 px
           :width: 400 px
           :alt: Form Agregar Autor
           :align: center 
    '''
    articulo = get_object_or_404(Articulo, slug=slug)
    form = Autor_Form(request.POST)
    autor_html = ""
    if form.is_valid():
        autor = Autor.objects.filter(nombre__iexact=form.cleaned_data.get('nombre'), \
                                     apellido_primero__iexact=form.cleaned_data.get('apellido_primero'), apellido_segundo__iexact=form.cleaned_data.get('apellido_segundo')).first()     
        if not autor: 
            autor = form.save()             
        if not articulo.autores.filter(pk=autor.pk).exists():
            articulo.autores.add(autor)
            autor_html = "<div class='col-lg-9 col-lg-offset-2' id='" + str(articulo.id) + "_" + str(autor.id) + "'> \
            <div class='input-group'><input type='text' class='form-control' id='inputAutor' value='" + \
            title(autor.nombre + " " + autor.apellido_primero + " " + autor.apellido_segundo) + \
                "'> <span class='input-group-addon glyphicon glyphicon-trash' title='Eliminar Autor' onclick='eliminarAutor(" + str(autor.id) + ", " \
                + str(articulo.id) + ", this)'></span></div></div>"
        else:    
            autor_html = "<h4 class='text-center col-lg-9 col-lg-offset-2 alert alert-danger alert-dismissible' role='alert'> \
            <button type='button' class='close' data-dismiss='alert' aria-label='Cerrar'><span aria-hidden='true'>&times;</span></button>El autor ingresado ya existe en el artículo.</h4>"  
            
    else:
        autor_html = "<h4 class='text-center col-lg-9 col-lg-offset-2 alert alert-danger alert-dismissible' role='alert'> \
        <button type='button' class='close' data-dismiss='alert' aria-label='Cerrar'><span aria-hidden='true'>&times;</span></button>Datos Ingresados Incorrectos.</h4>"  
    return {'append-fragments': {'#autores': autor_html, }, }


@ajax    
def guardar_palabraClave(request, slug):
    '''
        .. image:: _images/agregarPalabraClave.png 
           :height: 150 px
           :width: 350 px
           :alt: Agregar Palabra Clave
           :align: center
    
        Esta vista es muy similar a la de agregar autor, solo que en este caso se encarga de asociar al artículo las 
        palabras clave indicadas por el usuario mediante el formulario respectivo en la interfaz web.  
        Si la palabra clave indicada ya existe en el sistema lo que se hace solamente es asociarla al artículo, sino se crea 
        una nueva y se realiza la asociación.  Esta vista también utiliza ajax para indicar la respuesta al usuario y evitar 
        la recarga innecesaria de toda la página cada vez que se agrega una nueva palabra clave al artículo.
        
        .. note:: El formulario que permite al usuario agregar palabras clave desde la interfaz es el que se muestra en la \
        siguiente imagen.  La palabra ingresada en él es la que se recibe como parámetro del POST en esta vista.
        
        .. image:: _images/form_agregarPalabraClave.png 
           :height: 150 px
           :width: 350 px
           :alt: Form Agregar Palabra Clave
           :align: center 
    '''
    articulo = get_object_or_404(Articulo, slug=slug)
    form = PalabraClave_Form(request.POST)
    html = ""
    if form.is_valid():
        palabraClave = Palabra_clave.objects.filter(descripcion__iexact=form.cleaned_data.get('descripcion')).first()
        if not palabraClave:
            palabraClave = form.save()
        if not articulo.palabras_claves.filter(pk=palabraClave.pk).exists():
            articulo.palabras_claves.add(palabraClave)
            html = "<div class='col-lg-8 col-lg-offset-3' id='" + str(articulo.id) + "_" + str(palabraClave.id) + "'> \
                     <div class='input-group'><input type='text' class='form-control' id='inputAutor' value='" + \
                    title(palabraClave.descripcion) + \
                "'> <span class='input-group-addon glyphicon glyphicon-trash' title='Eliminar PalabraClave' onclick='eliminarPalabraClave(" + str(palabraClave.id) + ", " \
                + str(articulo.id) + ", this)'></span></div></div>"
        else:
            html = "<h4 class='text-center col-lg-9 col-lg-offset-2 alert alert-danger alert-dismissible' role='alert'> \
            <button type='button' class='close' data-dismiss='alert' aria-label='Cerrar'><span aria-hidden='true'>&times;</span></button>La palabra clave ingresada ya existe en el artículo.</h4>"  
    else:
        html = "<h4 class='text-center col-lg-9 col-lg-offset-2 alert alert-danger alert-dismissible' role='alert'> \
        <button type='button' class='close' data-dismiss='alert' aria-label='Cerrar'><span aria-hidden='true'>&times;</span></button>Error de Guardado.</h4>"  
    return {'append-fragments': {'#palabras': html, }, }


def eliminar_autor(request, autor, articulo):
    '''
        .. image:: _images/borrarAutor.png 
           :height: 110 px
           :width: 350 px
           :alt: Eliminar Autor
           :align: center
    
        Esta vista es la encargada de desligar autores con los artículos.  Esta vista es invocada cuando el usuario en la 
        interfaz selecciona la eliminación de un autor específico asociado al artículo que está revisando.
        Los ids del autor y el artículo son recibidos como parámetro, esto permite filtrar y seleccionar el autor y artículos 
        respectivos y hacer la correcta "eliminación".
        
        .. note:: Los autores en realidad no se eliminan ya que pueden tener más de un artículo asociado y se perderían datos \
        importantes en el sistema.
    '''
    autor = get_object_or_404(Autor, id=autor)
    articulo = get_object_or_404(Articulo, id=articulo)
    articulo.autores.remove(autor)
    return HttpResponse("True")

def eliminar_palabraClave(request, palabra, articulo):
    '''
        .. image:: _images/borrarPalabraClave.png 
           :height: 150 px
           :width: 350 px
           :alt: Eliminar Palabra Clave
           :align: center
           
        Esta vista es parecida a la vista de eliminar autor, solamente que se encarga de desligar palabras claves con artículos
        en el caso de que el usuario así lo desee.  También recibe como parámetro los ids de la palabra clave y el artículo para
        poder realizar la búsqueda de dichos elementos en la base de datos y poder hacer la "eliminación" correctamente.
        
        .. note:: Igual que en el caso de autores, las palabras clave en realidad no son eliminadas por completo del sistema \
        sino que solamente son desligadas del artículo, esto debido a que también pueden estar asociadas a más de un artículo \
        y no se quiere perder información importante en el sistema.
    '''
    palabra = get_object_or_404(Palabra_clave, id=palabra)
    articulo = get_object_or_404(Articulo, id=articulo)
    articulo.palabras_claves.remove(palabra)
    return HttpResponse("True")

def avalar_articulo(request, slug):
    '''
        .. image:: _images/avalarArticulo.png 
           :height: 150 px
           :width: 350 px
           :alt: Avalar Publicación
           :align: center
           
        Esta vista se encarga de realizar el aval del artículo para su publicación, se invoca en el caso de que el usuario
        seleccione Sí cuando se le pregunta si permite la publicación. Lo que se realiza es asignar el campo booleano que posee
        el artículo para indicar dicho aval en True. \n 
        Antes de realizar el aval se guarda la información del título y descripción que se reciben por POST, esto para salvar
        la información actualizada en el caso de que el asuario haya hecho alguna actualización de los metadatos del artículo
        y proporcionado información más completa de su contenido.
        Se le devuelve al usuario una página con un mensaje de agradecimiento.  
    '''
    articulo = get_object_or_404(Articulo, slug=slug)
    articulo.titulo = request.POST['titulo']
    articulo.descripcion = request.POST['descripcion']
    articulo.aval = True
    articulo.save()
    mensaje = "Muchas Gracias por permitirnos publicar su artículo en nuestro repositorio."
    return render(request, "agradecimiento.html", {'mensaje': mensaje})

def no_avalar_articulo(request, slug):
    '''
        .. image:: _images/noAvalarArticulo.png 
           :height: 150 px
           :width: 350 px
           :alt: No Avalar Publicación
           :align: center
        
        Esta vista es la encargada de eliminar el artículo de nuestro repositorio en el caso de que el usuario autor no brinde
        el aval de publicación.  Se invoca cuando se indica que No en la pregunta de autorización.  El artículo es eliminado por
        completo de nuestra base de datos.  Se le envía un mensaje de agradecimiento como respuesta al usuario.   
           
    '''
    articulo = get_object_or_404(Articulo, slug=slug)
    articulo.delete() 
    mensaje = "Muchas Gracias por su tiempo. Solicitamos disculpas por las molestias que pudimos haber causado."
    return render(request, "agradecimiento.html", {'mensaje': mensaje})


def no_autor(request, slug, autor):
    '''
        .. image:: _images/noAutor.png 
           :height: 150 px
           :width: 350 px
           :alt: No Autor
           :align: center
           
        Esta vista se encarga de resolver el error en el autor, esto en el caso de que se le informe a la persona incorrecta
        de la autoría de un artículo (lo que sucede generalmente en el caso de que tengan el mismo nombre y se eligió a la 
        persona incorrecta al buscar en la base de datos del TSE).  Se invoca en el caso de que el usuario autor indique que 
        en realidad él no es el autor. Se recibe como parámetro el id del autor que está realizando la acción. \n
        En este caso lo que se realiza es eliminar la relación con dicho autor en el artículo y en el caso de que el artículo
        se quede sin autores (porque es el único autor o todos los demás ya han sido eliminados) se elimina también el artículo
        de la base de datos.  Se retorna un mensaje de agradecimiento al usuario por la información brindada.
    '''
    articulo = get_object_or_404(Articulo, slug=slug)
    autor = get_object_or_404(Autor, id=autor)
    articulo.autores.remove(autor)
    if articulo.autores.count() == 0:
        articulo.delete()
    mensaje = "Muchas Gracias por su tiempo y por la información brindada."    
    return render(request, "agradecimiento.html", {'mensaje': mensaje})


