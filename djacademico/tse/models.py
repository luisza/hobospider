from django.db import models
from django.utils.six import python_2_unicode_compatible

# Create your models here.
@python_2_unicode_compatible
class PersonaTse(models.Model):
    cedula = models.CharField(max_length=15)
    nombre = models.CharField(max_length=50, db_index=True)
    apellido_primero = models.CharField(max_length=50)
    apellido_segundo = models.CharField(max_length=50)

    def __str__(self):
        return self.cedula

    class Meta:
        index_together = ['nombre', 'apellido_primero', 'apellido_segundo']