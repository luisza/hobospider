# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tse', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personatse',
            name='nombre',
            field=models.CharField(max_length=50, db_index=True),
        ),
        migrations.AlterIndexTogether(
            name='personatse',
            index_together=set([('nombre', 'apellido_primero', 'apellido_segundo')]),
        ),
    ]
