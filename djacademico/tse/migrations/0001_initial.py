# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PersonaTse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cedula', models.CharField(max_length=15)),
                ('nombre', models.CharField(max_length=50)),
                ('apellido_primero', models.CharField(max_length=50)),
                ('apellido_segundo', models.CharField(max_length=50)),
            ],
        ),
    ]
